# initializr-plugin

#### 介绍
基于Mybatis-plus的生成器写的gradle插件，用于初始化spring boot项目

#### 软件架构
软件架构说明


#### 安装教程

参考example的buil.gradle.kts文件，在插件中引入

plugins {
    id("com.jxpanda.initializr") version "版本号"
}

#### 使用说明

引入后编写init.yml文件，然后在tasks中找到initializr
使用generate生成代码（目前仅支持Java代码，不支持kotlin）
使用refresh刷新entity类（不影响service类，当数据结构有变更的时候使用）
