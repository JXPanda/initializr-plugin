plugins {
    java

    id("io.freefair.lombok") version "8.4"
    id("com.jxpanda.initializr") version "1.3.0"
}

group = "com.jxpanda.plugin"
version = "1.0.0"

object Version {
    const val PANDA_COMMONS = "1.2.8"
    const val MYBATIS_PLUS = "3.5.3.1"
    const val MYBATIS_PLUS_GENERATOR = "3.5.2"
    const val MYSQL_CONNECTOR = "8.0.25"
    const val FREEMARKER = "2.3.31"
    const val JACKSON = "2.14.2"
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("com.jxpanda.commons:commons-base:${Version.PANDA_COMMONS}")
    implementation("io.swagger:swagger-annotations:1.6.2")
    implementation("com.baomidou:mybatis-plus-annotation:${Version.MYBATIS_PLUS}")
    runtimeOnly("mysql:mysql-connector-java:${Version.MYSQL_CONNECTOR}")
}

configure<JavaPluginExtension> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}