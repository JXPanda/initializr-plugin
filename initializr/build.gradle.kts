@file:Suppress("UnstableApiUsage")

plugins {
    kotlin("jvm") version "1.9.21"
//    id("java-gradle-plugin")
    id("com.gradle.plugin-publish") version "1.2.1"
    `java-gradle-plugin`
    `java-library`
}

object Project {
    const val VERSION = "1.3.4"
    const val DESCRIPTION = "A plugin base on mybatis-plus-generator"
}

object Version {
    const val MYBATIS_PLUS = "3.5.3.1"
    const val MYBATIS_PLUS_GENERATOR = "3.5.2"
    const val MYSQL_CONNECTOR = "8.0.25"
    const val FREEMARKER = "2.3.31"
    const val JACKSON = "2.14.2"
}

group = "com.jxpanda.plugin"
version = Project.VERSION

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("com.baomidou:mybatis-plus-generator:${Version.MYBATIS_PLUS_GENERATOR}")
    // https://mvnrepository.com/artifact/com.baomidou/mybatis-plus-core
    implementation("com.baomidou:mybatis-plus-core:${Version.MYBATIS_PLUS}")
    implementation("org.freemarker:freemarker:${Version.FREEMARKER}")
    implementation("com.fasterxml.jackson.core:jackson-annotations:${Version.JACKSON}")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:${Version.JACKSON}")
    runtimeOnly("mysql:mysql-connector-java:${Version.MYSQL_CONNECTOR}")
}

gradlePlugin(configure = Action {
    website = "https://www.jxpanda.com"
    vcsUrl = "https://gitee.com/JXPanda/initializr-plugin.git"
    description = Project.DESCRIPTION
    plugins {
        create("initializr") {
            id = "com.jxpanda.initializr"
            displayName ="com.jxpanda.initializr"
            implementationClass = "com.jxpanda.plugin.initializr.InitializrPlugin"
            description = Project.DESCRIPTION
            version = Project.VERSION
            tags.set(listOf("mybatis-plus", "generator", "initializr"))
        }
    }
})


//pluginBundle(configure = Action {
//    website = "https://www.jxpanda.com"
//    vcsUrl = "https://gitee.com/JXPanda/initializr-plugin.git"
//    description = Project.DESCRIPTION
//    tags = arrayListOf("mybatis-plus", "generator", "initializr")
//})

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_17.majorVersion
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_17.majorVersion
    }
}