package com.jxpanda.plugin.initializr

import com.jxpanda.plugin.initializr.task.*
import org.gradle.api.Plugin
import org.gradle.api.Project
import java.util.*

private const val GROUP = "initializr"

@Suppress("PrivatePropertyName")
private val TASK_LIST = listOf(
    Initialize::class.java,
    Generate::class.java,
    Refresh::class.java,
    Xml::class.java,
    Controller::class.java
)

class InitializrPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        TASK_LIST.forEach { task ->
            project.tasks.create(task.simpleName.replaceFirstChar { it.lowercase(Locale.getDefault()) }, task).group =
                GROUP
        }
    }
}