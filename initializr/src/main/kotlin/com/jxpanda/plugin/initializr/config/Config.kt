package com.jxpanda.plugin.initializr.config

import com.fasterxml.jackson.annotation.JsonProperty

@Suppress("MemberVisibilityCanBePrivate")
class Config {
    lateinit var generator: Generator

    class Generator {
        lateinit var `package`: String

        @JsonProperty("data-source")
        lateinit var dataSource: DataSource

        val tables: Tables = Tables()

        val model: Model = Model()
    }

    class DataSource {
        lateinit var host: String
        lateinit var port: String
        lateinit var database: String
        lateinit var username: String
        lateinit var password: String
        var url: String = ""
            get() {
                field =
                    "jdbc:mysql://$host:$port/$database?characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true"
                return field
            }

    }

    class Tables {
        var include: List<String> = emptyList()
        var exclude: List<String> = emptyList()
    }

    class Model {
        @JsonProperty("prefixes")
        val prefixes: List<String> = listOf()

        @JsonProperty("super-class")
        val superClass: SuperClass = SuperClass()

        @JsonProperty("super-entity-columns")
        val superEntityColumns: List<String> = listOf(
            "id",
            "created_date",
            "updated_date",
            "deleted_date",
            "version",
            "creator_id",
            "updater_id"
        )

        @JsonProperty("logic-delete-column-name")
        val logicDeleteColumnName: String = "deleted_date"

        @JsonProperty("json-type-handler")
        val jsonTypeHandler: String = "com.jxpanda.commons.base.EntityJsonTypeHandler"

        @JsonProperty("import-package")
        val importPackage: ImportPackage = ImportPackage()
    }

    class SuperClass {
        val entity: String = "com.jxpanda.commons.base.Entity"
        val mapper: String = "com.jxpanda.commons.base.Mapper"
        val service: String = "com.jxpanda.commons.base.Service"

        @JsonProperty("service-impl")
        val serviceImpl: String = "com.jxpanda.commons.base.ServiceImpl"
    }

    class ImportPackage {
        val seeker: String = "com.jxpanda.commons.base.Seeker"
        val response: String = "com.jxpanda.commons.base.Response"
        val pagination: String = "com.jxpanda.commons.base.Pagination"
    }

}