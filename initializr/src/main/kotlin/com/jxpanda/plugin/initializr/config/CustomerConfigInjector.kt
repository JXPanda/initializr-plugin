package com.jxpanda.plugin.initializr.config

import com.baomidou.mybatisplus.generator.config.po.TableInfo


private const val CFG_KEY_IMPORT_PACKAGE = "importPackage"

/**
 * 自定义配置注入
 * */
class CustomerConfigInjector : InjectorConfig.Injector() {


    override fun buildInjectMap(tableInfo: TableInfo): Map<String, Any> {
        return mapOf(CFG_KEY_IMPORT_PACKAGE to model.importPackage)
    }


}