package com.jxpanda.plugin.initializr.config

import com.baomidou.mybatisplus.generator.config.InjectionConfig
import com.baomidou.mybatisplus.generator.config.PackageConfig
import com.baomidou.mybatisplus.generator.config.po.TableInfo

const val CFG_KEY_INJECT_PACKAGE = "injectPackage"

class InjectorConfig(
    private val packageConfig: PackageConfig,
    private val model: Config.Model
) : InjectionConfig() {

    private val injectors: MutableList<Injector> = mutableListOf()

    override fun beforeOutputFile(tableInfo: TableInfo, objectMap: MutableMap<String, Any>) {
        injectors.forEach { objectMap.putAll(it.buildInjectMap(tableInfo)) }
    }

    fun addInjector(injector: Injector): InjectorConfig {
        injector.packageConfig = packageConfig
        injector.model = model
        this.injectors.add(injector)
        return this
    }

    abstract class Injector {
        lateinit var packageConfig: PackageConfig
        lateinit var model: Config.Model
        abstract fun buildInjectMap(tableInfo: TableInfo): Map<String, Any>
    }

}