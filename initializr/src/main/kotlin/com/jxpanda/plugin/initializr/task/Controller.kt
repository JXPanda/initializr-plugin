package com.jxpanda.plugin.initializr.task

import com.baomidou.mybatisplus.generator.AutoGenerator
import com.baomidou.mybatisplus.generator.config.TemplateType

open class Controller : GeneratorTask() {

    override fun before(autoGenerator: AutoGenerator): AutoGenerator {
        return autoGenerator.apply {
            this.template.disable(
                TemplateType.XML,
                TemplateType.SERVICE,
//                TemplateType.SERVICE_IMPL,
                TemplateType.ENTITY,
                TemplateType.MAPPER
            )
        }
    }


}