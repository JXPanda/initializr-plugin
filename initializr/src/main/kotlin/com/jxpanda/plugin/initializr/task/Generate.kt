package com.jxpanda.plugin.initializr.task

import com.baomidou.mybatisplus.generator.AutoGenerator
import com.baomidou.mybatisplus.generator.config.TemplateType

open class Generate : GeneratorTask() {

    override fun before(autoGenerator: AutoGenerator): AutoGenerator {
        return autoGenerator.apply {
            this.template.disable(TemplateType.CONTROLLER)
        }
    }


}