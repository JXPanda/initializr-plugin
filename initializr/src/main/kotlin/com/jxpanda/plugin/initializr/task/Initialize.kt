package com.jxpanda.plugin.initializr.task

import com.baomidou.mybatisplus.generator.AutoGenerator
import com.baomidou.mybatisplus.generator.config.TemplateType

/**
 * 初始化逻辑是要生成一些配置文件
 * */
open class Initialize : GeneratorTask() {

    override fun before(autoGenerator: AutoGenerator): AutoGenerator {
        return autoGenerator
    }

}