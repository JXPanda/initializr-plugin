package com.jxpanda.plugin.initializr.task

import com.baomidou.mybatisplus.generator.AutoGenerator
import com.baomidou.mybatisplus.generator.config.TemplateType

/**
 * 刷新逻辑是不覆盖Service和ServiceImpl
 * */
open class Refresh : GeneratorTask() {

    override fun before(autoGenerator: AutoGenerator): AutoGenerator {
        return autoGenerator.apply {
            this.template.disable(TemplateType.SERVICE, TemplateType.CONTROLLER, TemplateType.XML)
        }
    }

}