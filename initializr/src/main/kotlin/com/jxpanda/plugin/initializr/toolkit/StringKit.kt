@file:Suppress("MemberVisibilityCanBePrivate")

package com.jxpanda.plugin.initializr.toolkit

import java.util.*
import java.util.stream.Collectors

private const val DASH: String = "_"
private const val BLANK: String = ""

open class StringKit {

    companion object {
        /**
         * 字符串首字母大写
         */
        fun capitalize(string: String): String {
            return if (string.isBlank() || Character.isUpperCase(string[0])) {
                string
            } else {
                string.substring(0, 1).toUpperCase() + string.substring(1)
            }
        }

        /**
         * 字符串首字母小写
         */
        fun uncapitalize(string: String): String {
            return if (string.isBlank() || Character.isLowerCase(string[0])) {
                string
            } else {
                string.substring(0, 1).toLowerCase() + string.substring(1)
            }
        }

        /**
         * 把字符串转为camelCase命名规范
         *
         * @param string 待转换字符串
         * @return 转换后的字符串
         */
        fun camelCase(string: String): String {
            val camelString = Arrays.stream<String>(string.split(DASH).toTypedArray())
                .map<String> { capitalize(it) }
                .collect(Collectors.joining(BLANK))
            return uncapitalize(camelString)
        }
    }


}