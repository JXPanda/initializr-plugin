import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    java
    id("io.freefair.lombok") version "5.0.0"
    id("org.springframework.boot") version "2.3.0.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
}



group = Project.GROUP
version = Project.VERSION

java {
    sourceCompatibility = JavaVersion.VERSION_11
}

repositories {
    Repositories.setRepositories(this)
}

// 去除log4j的依赖，这个包重复了
configurations {
    all {
        exclude(module = "log4j-api")
        exclude(module = "log4j-to-slf4j")
        resolutionStrategy {
            cacheChangingModulesFor(0, TimeUnit.SECONDS)
        }
    }
}


dependencies {
    implementation(Library.PANDA_COMMONS)
    implementation(Library.JJWT)
    implementation(Library.SWAGGER2)
    implementation(Library.SWAGGER_UI)
    // 以下是spring的依赖
    implementation(SpringLibrary.SPRING_BOOT)
    implementation(SpringLibrary.WEB)
    implementation(SpringLibrary.WEBFLUX)
    implementation(SpringLibrary.VALIDATION)
}

dependencies {
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<BootJar> {
    archiveBaseName.set("application")
    archiveVersion.set("")
}