object Version {
    /**
     * Panda的二方库，主要封装了常用的工具类的实体类
     * 内部依赖了jackson，所以这边不用显式的再依赖jackson了
     * */
    const val PANDA_COMMONS = "1.0.0-SNAPSHOT"
    const val JJWT = "0.9.1"
    const val SWAGGER2 = "2.9.2"
}

object Library {

    /**
     * jjwt是一个jwt的序列化和反序列化工具包
     * 实现了jwt的签发和验签功能
     * */
    const val JJWT = "io.jsonwebtoken:jjwt:${Version.JJWT}"

    /**
     * 公共库，封装了实体、服务的基类
     * 封装了常用的工具类
     * */
    const val PANDA_COMMONS = "com.jxpanda.commons:commons-base:${Version.PANDA_COMMONS}"

    const val SWAGGER2 = "io.springfox:springfox-swagger2:${Version.SWAGGER2}"
    const val SWAGGER_UI = "io.springfox:springfox-swagger-ui:${Version.SWAGGER2}"

}

/**
 * spring的依赖
 * 分开写是为了避免眼花
 * */
object SpringLibrary {
    const val SPRING_BOOT = "org.springframework.boot:spring-boot-starter"
    const val VALIDATION = "org.springframework.boot:spring-boot-starter-validation"
    const val WEB = "org.springframework.boot:spring-boot-starter-web"
    const val WEBFLUX = "org.springframework.boot:spring-boot-starter-webflux"
}
