import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.kotlin.dsl.maven

object Repositories {

    private const val DOMAIN = "https://nexus.jxpanda.com"
    private const val ALIYUN = "https://maven.aliyun.com/repository/public"
    private const val URL_RELEASE = "$DOMAIN/repository/maven-releases/"
    private const val URL_SNAPSHOTS = "$DOMAIN/repository/maven-snapshots/"
    private const val USERNAME = "ezor"
    private const val PASSWORD = "Ezor2019@)!("

    fun setRepositories(handler: RepositoryHandler) {
        handler.mavenLocal()
        handler.maven(ALIYUN)
        handler.maven(URL_RELEASE) {
            credentials {
                username = USERNAME
                password = PASSWORD
            }
        }
        handler.maven(URL_SNAPSHOTS) {
            credentials {
                username = USERNAME
                password = PASSWORD
            }
        }
    }

}