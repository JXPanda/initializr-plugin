package cn.floravita.manager.user.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jxpanda.commons.toolkit.JsonKit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonConfig {

    @Bean
    public ObjectMapper jackson() {
        return JsonKit.jackson();
    }

}
