package cn.floravita.manager.user.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
@Profile("dev")
public class Swagger2Config {


    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("花叙花房")
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.floravita.manager.user.api"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(Collections.singletonList(
                        new ParameterBuilder()
                                .name("Authorization")
                                .description("登陆成功后签发的Token，除了授权类（auth）接口外，所有接口都需要携带Token才能访问")
                                .modelRef(new ModelRef("string"))
                                .parameterType("header")
                                .required(false)
                                .build()
                ));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Document")
                .description("Document for floravita")
                .termsOfServiceUrl("https://www.baidu.com")
                .version("1.0.0")
                .contact(new Contact("Panda", "https://www.baidu.com", "239692275@qq.com"))
                .build();
    }

}
