<#assign servicePropertyName = table.serviceName?uncap_first>
<#assign entityPropertyName = entity?uncap_first>
package ${package.Controller};


import org.springframework.web.bind.annotation.*;
import ${importPackage.pagination};
import ${importPackage.response};
import ${importPackage.seeker};
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
<#if entityLombokModel>
import lombok.AllArgsConstructor;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
<#if entityLombokModel>
@AllArgsConstructor
</#if>
@RequestMapping("${table.entityPath}")
@Tag(name = "${table.comment!}", description = "${table.comment!}API")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    private final ${table.serviceName} ${servicePropertyName};

    @PostMapping("/save")
    @Operation(summary = "保存", description = "传入对象，有id更新，没有id创建")
    public Mono<Response<Boolean>> save(@RequestBody ${entity} ${entityPropertyName}) {
        return Mono.just(${servicePropertyName}.saveOrUpdate(${entityPropertyName}))
                .map(Response::ok);
    }

    @DeleteMapping("/delete/{id:\\d+}")
    @Operation(summary = "删除", description = "传入id")
    public Mono<Response<Boolean>> delete(@PathVariable("id") String id) {
        return Mono.just(${servicePropertyName}.removeById(id))
                .map(Response::ok);
    }

    @GetMapping("/detail/{id:\\d+}")
    @Operation(summary = "详情", description = "传入id")
    public Mono<Response<${entity}>> detail(@PathVariable("id") String id) {
        return Mono.just(${servicePropertyName}.getById(id))
                .map(Response::ok);
    }

    @PostMapping("/query")
    @Operation(summary = "查询", description = "传入查询对象")
    public Mono<Response<Pagination<${entity}>>> query(@RequestBody Seeker<${entity}> seeker) {
        return Mono.just(${servicePropertyName}.pagination(seeker))
                .map(Response::ok);
    }

}
</#if>
